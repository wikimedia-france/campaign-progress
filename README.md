Campaign progress
===================

A progress tracking Widget for Wikimédia France’s donation campaign.

# Install
* copy `settings.php.sample` to `settings.php` and fill the parameters
* Allow IP address of your server in iRaiser security parameters
* start the webserver

# Use
Create a text widget with the following content:
```php
<p>
    <iframe src="https://campaign-progress.wikimedia.fr/donations-iframe-circle.php" width="250px" height="320px" frameborder="0"></iframe>
</p>
```

# Caching
The if `$DEBUG` is set to `0`, there will be a 5-minute caching of the page, to avoid flooding the iRaiser API with queries and getting IP-banned in the process.