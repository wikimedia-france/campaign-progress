<?php
class CampaignThermometer { 
    public $api;
    public $shift;
    public $goal_amount;
    public $year;

    public $current_amount;
    public $data_available;
    public $percentage;

    public $delay;
    public $days;
    public $hours;
    public $minutes;
    public $seconds;


    function __construct($param_root, $param_user, $param_pwd, $shift, $goal_amount, $year) {
        $this->api = array();
        $this->api['base'] = $param_root;
        $this->api['login'] = 'user_api=' . $param_user . '&pwd_api=' . $param_pwd;
        $this->shift = $shift;
        $this->goal_amount = $goal_amount;
        $this->year = $year;
        $this->current_amount = 0;
        $this->data_available = 0;
        $this->percentage = 0;

        # Countdown
        $this->date = strtotime($this->year . "-12-31 23:59:59 CET");
        $this->delay = $this->date - time();
        $this->days = floor($this->delay / 86400);
        $this->hours = floor(($this->delay % 86400)/ 3600);
        $this->minutes = floor(($this->delay % 3600)/ 60);
        $this->seconds = floor($this->delay % 60);
    }

	public function get_amount($campaign) {
      $this->api['params'] = $this->api['login'] . '&frequency=once&campaigns[]=' . $campaign;
      $api_call = file_get_contents($this->api['base'] . $this->api['params']);

      $api_return = explode(';', $api_call);

      if ($api_return[0] == 1) {
          $api_result = explode('|', $api_return[1]);
          $this->current_amount += round($api_result[1]/100, 0); 
          $this->data_available = 1;
      }
    }

    public function get_total_amount($campaigns) {
        foreach ($campaigns as $c) {
            $this->get_amount($c);
        }
        $this->current_amount -= $this->shift;
        $this->percentage = round($this->current_amount/$this->goal_amount*100, 0);
    }

    public function show_current_amount() {
        return number_format($this->current_amount, 0, ',', ' ');
    }

    public function show_remaining_hourmin() {
        return sprintf("%02d", $this->hours) . ':' . sprintf("%02d", $this->minutes);
    }
}