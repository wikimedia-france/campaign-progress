<?php

include_once('settings.php');

if (!$DEBUG) {
  include('top-cache.php'); 
}

# API call
$api_base = $api_root;
$api_login = 'user_api=' . $user_api . '&pwd_api=' . $pwd_api;
$api_params = $api_login . '&frequency=once&campaigns[]=' . $campaign;

$api_call = file_get_contents($api_base . $api_params);


$api_return = explode(';', $api_call);

if ($api_return[0] == 1) {
    $api_result = explode('|', $api_return[1]);
    $current_amount = round($api_result[1]/100, 0) - $shift; 
    $percentage = round($current_amount/$goal_amount*100, 0);
    $data_available = 1;
} else {
    $data_available = 0;
}

# Countdown
$date = strtotime($year . "-12-31 23:59:59 CET");
$delay = $date - time();
$days = floor($delay / 86400);
$hours = floor(($delay % 86400)/ 3600);
$minutes = floor(($delay % 3600)/ 60);
$seconds = floor($delay % 60);


//*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Soutenez-nous !</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="lib/bootstrap-4.5.0-dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="iframe.css" />
    <link rel="stylesheet" href="circle.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div id="donations-thermometer">
  		<h3>On a besoin de vous !</h3>
<?php 

if ($data_available) { ?>
		<div class="progresscircle" data-percentage=<?php echo $percentage; ?>>
			<span class="progresscircle-left">
				<span class="progresscircle-bar"></span>
			</span>
			<span class="progresscircle-right">
				<span class="progresscircle-bar"></span>
			</span>
			<div class="progresscircle-value">
				<div>
					<img src="img/logo-wikimedia.png" />
				</div>
			</div>
		</div>

    <div class="text-center">
      <span class="font-weight-bold text-primary"><?php echo number_format($current_amount, 0, ',', ' '); ?></span> / <?php echo number_format($goal_amount, 0, ',', ' '); ?> €
    </div>

  		<div class="row text-center mt-4">
          <div class="col-6 border-right">
            <div class="h4 font-weight-bold mb-0 text-primary"><?php echo $percentage ?>%</div><span class="small text-gray">atteints</span>
          </div>
          <?php } ?>

          <?php if (time() < $date) { ?>
          <div class="col-6">
            <?php if ($days > 0) { ?>
            <div class="h4 font-weight-bold mb-0 text-primary"><?php echo $days ?></div><span class="small text-gray">jours restants</span>
          <?php } else { ?>
            <div class="h4 font-weight-bold mb-0 text-primary"><?php echo sprintf("%02d", $hours) . ':' . sprintf("%02d", $minutes) ?></div><span class="small text-gray">restantes</span>
          <?php } ?>
          </div>
        </div>
      <?php } ?>

  		<p class="calltoaction">Wikimédia France ne vit que grâce à vos dons ! Pour que nous puissions continuer à soutenir la connaissance libre en <?php echo($year+1) ?>,
  		<strong><a href="http://dons.wikimedia.fr/soutenez-nous/" target="_blank" title="Soutenez-nous">soutenez-nous !</a></strong></p>
	</div>
	<script src="lib/jquery-3.5.1.min.js"></script>
	<script src="lib/bootstrap-4.5.0-dist/js/bootstrap.min.js"></script>
  </body>
</html>

<?php 
if (!$DEBUG) {
  include('bottom-cache.php'); 
}
?>
